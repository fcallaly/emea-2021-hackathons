# Deployment Hackathon

## Overview

The primary goal of this hackathon is to get the Java/Spring REST API that was created last week deploying to Openshift through a Jenkins pipeline.

The checklist below provides an outline of what is required to do this.

## Java App Checklist
1. Ensure properties in ```src/main/resources/application.properties``` can be overridden by environment variables. [example](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/master/src/main/resources/application.properties) 

2. You will need unit tests to pass on the CI server. Your unit tests should not depend on a particular database system being available. To facilitate this, use the "h2" database for tests
    - Add the h2 database to your pom.xml [example](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/af4ca0bb8c6392ceba27f921ea115cec87577e01/pom.xml#lines-51)
    - Add a properties file to src/**test**/resources/application.properties to ensure h2 is enabled for tests. This properties file should only have two lines as seen [here](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/master/src/test/resources/application.properties)
    - Any tests that actually interact with a database e.g. MockMvc tests add the annotation ```@AutoConfigureTestDatabase``` [example](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/af4ca0bb8c6392ceba27f921ea115cec87577e01/src/test/java/com/training/northwind/controller/ShipperControllerTest.java#lines-17)
    
3. Ensure you have the jacoco plugin in pom.xml [example](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/af4ca0bb8c6392ceba27f921ea115cec87577e01/pom.xml#lines-65)

## Files needed for the pipeline
1. Add a Dockerfile [example](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/master/Dockerfile)

2. Add a Jenkinsfile to define the pipeline [example](https://bitbucket.org/fcallaly/northwind-shippers-jpa/src/master/Jenkinsfile) (change the mysqlHost & projectName variables)


## Create a new pipeline task on Jenkins
1. Check the "build when a change is pushed to bitbucket" option

2. Give the git url of your bitbucket repository (ends with .git)

3. Test with "build now"

## Add a webhook to bitbucket
1. In your bitbucket repository, add a webhook to trigger the jenkins job.
    - Repository Settings->Webhooks->Add Webhook.
    - Title can be anything.
    - URL: http://<your_linux_hostname>:8080/bitbucket-hook/

