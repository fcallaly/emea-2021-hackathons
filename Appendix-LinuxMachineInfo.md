# Training Linux Machines

Each person has been allocated a linux machine. The "number" is the same as your windows VM number. We will refer to that number as **X**.

| Windows VM | Linux Machine |
|---|---|
|citiemeaX.congre.com | emeadockerX.conygre.com |

These machines will not power on automatically, if the machine is off then start it from [student.conygre.com](https://student.conygre.com) . The region is N. Virginia.

#### Linux Credentials
- username: grads
- password: <the usual password>

#### SSH Command
You can run ssh from powershell, gitbash, or your preferred ssh client.

```ssh grads@<linux-hostname>```

#### MySQL
MySQL is installed and running on each machine and is externally accessible. MySQL credentials are:

- username: conygre
- password: <ask instructor and make a note>

#### Jenkins
Jenkins is installed and running on each machine, the web ui url is:

```http://<your_linux_host>:8080```

- username: admin
- password: <the usual password>

The jenkins service can be restarted from the linux command line with systemctl:
```sudo systemctl restart jenkins```

#### Openshift
Openshift is installed and running on each machine, the web ui url is:

```https://<your_linux_host>:8443```   (The https is required! You will need to accept the unsigned https cert)

- credentials: admin/admin

The openshift service can be restarted from the linux command line with systemctl:
```sudo systemctl restart okd```

#### Docker Registry
There is a docker registry running in the training environment, it's hostname is ```dockerreg.conygre.com```.

The standard Jenkinsfile examples shown in training will push docker images to this registry.

To **view** the contents of the docker registry: [http://dockerreg.conygre.com:8080](http://dockerreg.conygre.com:8080)

To **push** a docker image to the registry tag your images with the prefix ```dockerreg.conygre.com:5000/```
